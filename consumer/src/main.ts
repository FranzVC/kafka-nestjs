import { NestFactory } from '@nestjs/core';
import {MicroserviceOptions, Transport} from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
 
const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
  transport: Transport.KAFKA,
  options: {
    client: {
      clientId:"consumer_1",
      brokers: ['kafka:9092'],
    },
    consumer: {
     groupId:'consumer-kafka',
    }
  }
});

   app.listen()
}
bootstrap();
