import { Injectable } from '@nestjs/common';
import {MessagePattern, Payload} from "@nestjs/microservices";

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  /**
   * @param message
   */
  showMessage(message) {
    console.log('message send from producer: ',message.value)
  }
}
