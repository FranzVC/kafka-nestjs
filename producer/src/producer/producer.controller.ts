import { Body, Controller, Get, Post } from '@nestjs/common';
import { ProducerService } from './producer.service';
import {ProducerMessageDTO} from "./producer.model";
import { ApiBody, ApiCreatedResponse, ApiResponse } from '@nestjs/swagger';

@Controller('/producer')
export class ProducerController {
  constructor(private readonly producerService: ProducerService) {}
  @Post('/')
  @ApiCreatedResponse({
    description: "Producer publish some message"
  })
  @ApiBody({type:ProducerMessageDTO})
  public async publishMessage(@Body() data: ProducerMessageDTO) {
    return await this.producerService.publish(data);
  }
}
