import { Module } from '@nestjs/common';
import { ProducerController } from './producer.controller';
import { ProducerService } from './producer.service';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'producer_name',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'client-id',
            brokers: ['kafka:9092'],
            //brokers: ['localhost:9093']
          },
          consumer: {
            groupId: 'consumer-group-id',
          },
        },
      },
    ]),
  ],
  controllers: [ProducerController],
  providers: [ProducerService],
})
export class ProducerModule {}
