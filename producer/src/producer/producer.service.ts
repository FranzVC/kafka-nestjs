import { Inject, Injectable } from '@nestjs/common';
import { ClientKafka } from '@nestjs/microservices';
import { ProducerMessageDTO } from './producer.model';

@Injectable()
export class ProducerService {
  /**
   * @param client
   */
  constructor(@Inject('producer_name') private readonly client: ClientKafka) {}
  /**
   * @param topic
   * @param message
   */
  public publish({ topic, message }: ProducerMessageDTO) {
    return this.client.emit(topic, message);
  }
}
