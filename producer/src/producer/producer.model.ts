import { ApiProperty } from '@nestjs/swagger';

export class ProducerMessageDTO {
  @ApiProperty({
    type: String,
    description: "The topic of message"
  })
  topic: string;

  @ApiProperty({
    type: String,
    description: "Message will be published"
  })
  message: any;
}
